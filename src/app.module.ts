import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

// 配置文件
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { file: envFilePath } = require('../config/env');

// 设置模块
import { SettingModule } from './modules/setting/setting.module';
import { Setting } from './modules/setting/setting.entity';

// 鉴权模块
import { AuthModule } from './modules/auth/auth.module';

// 用户模块
import { UserModule } from './modules/user/user.module';
import { User } from './modules/user/user.entity';

// 访问统计模块
import { ViewModule } from './modules/view/view.module';
import { View } from './modules/view/view.entity';

// 短信发送模块
import { SmsModule } from './modules/sms/sms.module';
import { Sms } from './modules/sms/sms.entity';

// 文件模块
import { FileModule } from './modules/file/file.module';
import { File } from './modules/file/file.entity';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true, envFilePath: [envFilePath] }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        entities: [User, View, Setting, Sms, File],
        host: configService.get('DB_HOST', '81.70.87.115'),
        port: configService.get<number>('DB_PORT', 3306),
        username: configService.get('DB_USER', 'nest-bug-server'),
        password: configService.get('DB_PASSWD', 'jjYskdkeGJ3ymN4L'),
        database: configService.get('DB_DATABASE', 'nest-bug-server'),
        charset: 'utf8mb4',
        timezone: '+08:00',
        synchronize: true,
      }),
    }),
    AuthModule,
    UserModule,
    ViewModule,
    SmsModule,
    FileModule,
    SettingModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
