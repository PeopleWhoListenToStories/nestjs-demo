// main.ts 中配置
import { NestFactory } from '@nestjs/core';
import * as rateLimit from 'express-rate-limit';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as bodyParser from 'body-parser';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ExcludeNullInterceptor } from './interceptors/transfrom.interceptor';
import { HttpExceptionFilter } from './filters/http-exception.filter';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { config: envConfig } = require('../config/env');

import { AppModule } from './app.module';
// api文档插件

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors(); // 启用 CORS
  app.setGlobalPrefix(envConfig.SWAGGER_ENDPOINT_PREFIX);
  app.use(helmet());
  app.use(compression()); // 开启 gzip压缩
  app.use(bodyParser.json({ limit: '10mb' })); // 修改请求的容量
  app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
  app.use(rateLimit({ max: 1000, windowMs: 15 * 60 * 1000 }));
  app.useGlobalInterceptors(new ExcludeNullInterceptor()); // 正常情况下，响应统一的返回值
  app.useGlobalFilters(new HttpExceptionFilter()); // 异常情况下， 响应统一的返回值

  // DocumentBuilder是一个辅助类，有助于结构的基本文件SwaggerModule。它包含几种方法，可用于设置诸如标题，描述，版本等属性。
  const options = new DocumentBuilder()
    .setTitle(envConfig.SWAGGER_UI_TITLE)
    .setDescription(envConfig.SWAGGER_UI_TITLE_DESC) // 文档介绍
    .setVersion(envConfig.SWAGGER_API_VERSION) // 文档版本
    .addTag('') // 每个tag标签都可以对应着几个@ApiUseTags('用户,安全') 然后被ApiUseTags注释，字符串一致的都会变成同一个标签下的
    // .setBasePath('http://localhost:5000')
    .build();
  // 为了创建完整的文档（具有定义的HTTP路由），我们使用类的createDocument()方法SwaggerModule。此方法带有两个参数，分别是应用程序实例和基本Swagger选项。
  const document = SwaggerModule.createDocument(app, options);
  // 最后一步是setup()。它依次接受（1）装入Swagger的路径，（2）应用程序实例, （3）描述Nest应用程序的文档。
  SwaggerModule.setup(envConfig.SWAGGER_SETUP_PATH, app, document, {
    customSiteTitle: '',
    swaggerOptions: {
      explorer: true,
      docExpansion: 'list',
      filter: true,
      showRequestDuration: true,
      syntaxHighlight: {
        active: true,
        theme: 'tomorrow-night',
      },
    },
  });
  // SwaggerModule.setup('/', app, document);
  await app.listen(envConfig.SERVE_LISTENER_PORT, () => {
    console.log(`listen run in 5000`);
  });
}

bootstrap();
