import * as TxyunOSS from 'cos-nodejs-sdk-v5';
import { OssClient } from './oss-client';

export class TxyunOssClient extends OssClient {
  private async buildClient() {
    const config = this.config;
    return new TxyunOSS({
      SecretId: config.SecretId as string,
      SecretKey: config.SecretKey as string, // 私钥
    });
    // return TxyunOSS.putObject(
    //   {
    //     Bucket: 'picture-1302857231' /* Bucket,名称 必须 */,
    //     Region: 'ap-beijing' /* 所属地域 必须 */,
    //     Key: filename /* 必须 */,
    //     Body: stream, // 上传文件对象
    //     onProgress: function (progressData) {
    //       console.log(JSON.stringify(progressData)) //返回信息，包括路径
    //     },
    //   },
    //   function (err, data) {
    //     resolve(data.Location)
    //   }
    // )
  }

  async putFile(filepath: string, buffer: any) {
    const client = await this.buildClient();
    // const { url } = await client.putObject(filepath, buffer);
    const data = await new Promise((resolve) => {
      client.putObject(
        {
          Bucket: 'picture-1302857231' /* Bucket,名称 必须 */,
          Region: 'ap-beijing' /* 所属地域 必须 */,
          Key: filepath /* 必须 */,
          Body: buffer, // 上传文件对象
          onProgress(progressData) {
            // tslint:disable-next-line:no-console
            console.log(JSON.stringify(progressData)); // 返回信息，包括路径
          },
        },
        // tslint:disable-next-line:no-shadowed-variable
        (err, data: TxyunOSS.PutObjectResult) => {
          resolve(data.Location);
        },
      );
    });
    return `https://${data}`;
    // return url;
  }

  async deleteFile(url: string) {
    console.log(
      `%c 🗽 🚀 : TxyunOssClient -> deleteFile -> url `,
      `font-size:14px;background-color:#d0c69d;color:black;`,
      url,
    );
    // const client = await this.buildClient();
    // await client.delete(url);
  }
}
