// 'use strict';
import { HttpException, HttpStatus } from '@nestjs/common';
import { SettingService } from '../modules/setting/setting.service';

// tslint:disable-next-line:no-var-requires
// eslint-disable-next-line @typescript-eslint/no-var-requires
const QcloudSms = require('qcloudsms_js');

const cfg = {
  appid: 1400492363, // SDK AppID 以1400开头
  appkey: '0bd1dd43b7ddbb2e7a0bce258e5ac10a', // 短信应用 SDK AppKey
  templateId: '1099706', // 短信模板 ID，需要在短信控制台中申请
  smsSign: '我把笔杆折断', // NOTE: 签名参数使用的是`签名内容`，而不是`签名ID`。这里的签名"腾讯云"只是示例，真实的签名需要在短信控制台申请
};

export class CloudSms {
  settingService: SettingService;
  config: Record<string, unknown>;

  constructor(settingService: SettingService) {
    this.settingService = settingService;
  }

  private async getConfig() {
    const data = await this.settingService.findAll(true);
    const config = JSON.parse(data.oss) || {};
    if (!config) {
      throw new HttpException(
        'OSS 配置不完善，无法进行操作',
        HttpStatus.BAD_REQUEST,
      );
    }
    return config as Record<string, unknown>;
  }

  private async getCloudConfig() {
    const config = await this.getConfig();
    const client = QcloudSms(config.appid, config.appkey); // 实例化 QcloudSms
    return client.SmsMultiSender();
  }

  async sendCloudSms(phoneList: string[], codeList: number[]) {
    const config = await this.getConfig();
    const client = await this.getCloudConfig();
    return new Promise((resolve) => {
      client.sendWithParam(
        86,
        phoneList, // 一个数组
        config.templateId, // 模版 id
        codeList, // 正文中的参数值
        config.smsSign, // 签名 未提供或者为空时，会使用默认签名发送短信
        '',
        '',
        (err, res, resData) => {
          // 回调函数
          if (!err) {
            resolve(resData);
          } else {
            // tslint:disable-next-line:no-console
            console.log('err: ', err, res, resData);
          }
        },
      );
    });
  }
}

export const CloudSendSms = async (phoneList: string[], codeList: number[]) => {
  const client = QcloudSms(cfg.appid, cfg.appkey); // 实例化 QcloudSms
  const msender = client.SmsMultiSender();
  const data = await new Promise((resolve) => {
    msender.sendWithParam(
      86,
      phoneList, // 一个数组
      cfg.templateId, // 模版 id
      codeList, // 正文中的参数值
      cfg.smsSign, // 签名 未提供或者为空时，会使用默认签名发送短信
      '',
      '',
      (err, res, resData) => {
        // 回调函数
        if (!err) {
          resolve(resData);
        } else {
          // tslint:disable-next-line:no-console
          console.log('err: ', err, res, resData);
        }
      },
    );
  });
  return data;
};
