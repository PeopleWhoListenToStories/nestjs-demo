import {
  Controller,
  HttpStatus,
  HttpCode,
  Post,
  Body,
  UseInterceptors,
  ClassSerializerInterceptor,
  UseGuards,
} from '@nestjs/common';
import { ApiHeader, ApiProperty, ApiTags, ApiOperation } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { Roles } from './roles.guard';
import { JwtAuthGuard } from './jwt-auth.guard';

class User {
  @ApiProperty({
    description: '用户名',
  })
  name: string;
  @ApiProperty({
    description: '密码',
  })
  password: string;
}

@ApiTags('Auth 模块')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /**
   * 用户登录
   * @param user
   */
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('login')
  @ApiOperation({
    tags: ['Auth'],
    description: '登录',
  })
  @HttpCode(HttpStatus.OK)
  async login(@Body() user: User) {
    const res = await this.authService.login(user);
    return res;
  }

  @Post('admin')
  @ApiOperation({
    tags: ['Auth'],
    description: '校验当前是否是管理员',
  })
  @Roles('admin')
  @UseGuards(JwtAuthGuard)
  @ApiHeader({
    name: 'Authoriation',
    required: true,
    description: '本次请求请带上token',
  })
  createBook() {
    return this.authService.checkAdmin();
  }
}
