import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, In, Repository } from 'typeorm';
import { Cron, Interval, Timeout } from '@nestjs/schedule';
import { dateFormat, dayFormat } from '../../utils/date.util';
import { Sms } from './sms.entity';
import { isMobile } from '../../utils/validate.util';
import { infoLogger } from '../../logger/index';
import { CloudSms, CloudSendSms } from '../../utils/cloudsms.utils';
import { SettingService } from '../setting/setting.service';

@Injectable()
export class SmsService {
  private cloudSms: CloudSms;

  private readonly logger = new Logger(SmsService.name);

  constructor(
    @InjectRepository(Sms)
    private readonly repository: Repository<Sms>,
    private readonly settingService: SettingService,
  ) {
    this.cloudSms = new CloudSms(this.settingService);
  }

  /**
   * 新增数据
   */
  async createUserSms(
    smsData: Partial<Sms> | Array<Partial<Sms>>,
  ): Promise<Sms[]> {
    if (!Array.isArray(smsData)) {
      smsData = [smsData];
    }
    // if (smsData.some((knowledge) => !knowledge.parentId)) {
    //   throw new HttpException('无效的知识库章节', HttpStatus.BAD_REQUEST);
    // }
    const result = [];
    for (const knowledge of smsData) {
      result.push(await this.createMobileSmsInfo(knowledge));
    }
    return result;
  }

  /**
   * 新建短信手机号列表
   * @param
   */
  async createMobileSmsInfo(smsData: Partial<Sms>): Promise<Sms> {
    const { mobile } = smsData;
    if (!isMobile(mobile)) {
      // 手机号考虑
      throw new HttpException('手机号格式不正确', HttpStatus.OK);
    }

    const exist = await this.repository.findOne({ where: { mobile } });
    if (exist && mobile) {
      // 手机号考虑
      throw new HttpException('手机号已存在', HttpStatus.OK);
    }

    // if (status === 'locked') {
    //   Object.assign(smsData, { publishAt: dateFormat() });
    // }

    const data = await this.repository.create(smsData);
    await this.repository.save(data);
    return data;
  }

  /**
   * 删除手机号
   */
  async deleteMobile(id) {
    const data = await this.repository.findOne(id);
    if (!data) {
      throw new HttpException('没有对应的数据', HttpStatus.BAD_REQUEST);
    }
    if (!data.id) {
      const query = this.repository
        .createQueryBuilder('sms')
        .where('sms.id=:id')
        .setParameter('id', data.id);
      const children = await query.getMany();
      if (children.length) {
        for (const item of children) {
          await this.repository.remove(item);
        }
      }
    }
    return this.repository.remove(data);
  }

  /**
   * 修改手机号
   */
  async updateMobile(id, data: Partial<Sms>): Promise<Sms> {
    const oldData = await this.repository.findOne(id);
    if (!oldData) {
      throw new HttpException('没有对应的数据', HttpStatus.BAD_REQUEST);
    }
    const newData = {
      ...data,
    };
    const result = await this.repository.merge(oldData, newData);
    await this.repository.save(result);
    return result;
  }

  /**
   * 获取所有手机号列表
   */
  async findAll(queryParams): Promise<[Sms[], number]> {
    const query = this.repository.createQueryBuilder('sms');
    const { page = 1, pageSize = 10, status, ...otherParams } = queryParams;
    query.skip((+page - 1) * +pageSize);
    query.take(+pageSize);
    if (status) {
      query.andWhere('sms.status=:status').setParameter('status', status);
    }
    if (otherParams) {
      Object.keys(otherParams).forEach((key) => {
        query
          .andWhere(`sms.${key} LIKE :${key}`)
          .setParameter(`${key}`, `%${otherParams[key]}%`);
      });
    }
    const [data, total] = await query.getManyAndCount();
    console.log(
      `%c 🔚 🚀 : SmsService -> data `,
      `font-size:14px;background-color:#cc2455;color:white;`,
      data,
    );
    return [data, total];
  }

  /**
   * 定时任务 每日发送短信
   */
  // @Cron('0 0 10 * * 1-5') // 周一至周五上午 10:00 AM
  // async sendEveryDaySms() {
  //   this.cloudSms = new CloudSms(this.settingService);
  //   const status = 'active'; // 激活状态的手机号
  //   const query = this.repository.createQueryBuilder('sms');
  //   query.andWhere('sms.status=:status').setParameter('status', status);
  //   const [data, total] = await query.getManyAndCount();
  //   if (data.length > 0) {
  //     const phoneList = data.map((target) => target.mobile);
  //     const dayInfo = [
  //       +dateFormat(null, 'MM'),
  //       +dateFormat(null, 'dd'),
  //       6 - new Date().getDay(),
  //       // +dayFormat('2021/09/21 00:00:00'),
  //       // +dayFormat('2021/10/01 00:00:00'),
  //       // +dayFormat('2022/01/01 00:00:00'),
  //       // +dayFormat('2022/02/01 00:00:00'),
  //     ];
  //     const clouldResult = await CloudSendSms(phoneList, dayInfo);
  //     this.logger.debug(clouldResult);
  //     const result = `${dayInfo[0]}月${dayInfo[1]}日
  //                     早上好，打工人！工作再累，一定不要忘记摸鱼哦 有事没事起身去茶水间去厕所去廊道走两步 ·
  //                     距离周末还有${dayInfo[2]}天 ·
  //                     世上有两种最耀眼的光芒，一种是太阳，一种是打工人努力的模样 · `;
  //     if (clouldResult) {
  //       infoLogger.info([phoneList, result, result]);
  //     }

  //     // const result = `
  //     // ${dayInfo[0]}月${dayInfo[1]}日
  //     // 早上好，打工人，工作再累，一定不要忘记摸鱼哦 有事没事起身去茶水间去厕所去廊道走走，别老在工位上坐着 ·
  //     // Life is your own The money belongs to the boss ·
  //     // Remember often go home to have a look ·
  //     // 距离中秋假期还有${dayInfo[2]}天 距离国庆假期还有${dayInfo[3]}天 距离跨年假期还有${dayInfo[4]}天 距离春节假期还有${dayInfo[5]}天`;
  //     // const clouldResult = CloudSendSms(phoneList, dayInfo);
  //   }
  // }

  /**
   * 定时任务 一分钟执行一次
   */
  @Cron('1 * * * * *')
  async sendEveryDaySmsOnMinute() {
    this.cloudSms = new CloudSms(this.settingService);
    const status = 'active'; // 激活状态的手机号
    const isTodaySend = 'locked'; // 当天未发送短信的状态
    const query = this.repository.createQueryBuilder('sms');
    query.andWhere('sms.status=:status').setParameter('status', status);
    query
      .andWhere('sms.isTodaySend=:isTodaySend')
      .setParameter('isTodaySend', isTodaySend);
    const [data] = await query.getManyAndCount();
    const idList = [];
    for (let i = 0; i < data.length; i++) {
      const item = data[i];
      const time = new Date(item.sendAt);
      const hours = time.getHours();
      const minutes = time.getMinutes();
      if (
        hours === new Date().getHours() &&
        minutes === new Date().getMinutes()
      ) {
        idList.push(item.id);
        const phoneList = data.map((target) => target.mobile);
        const dayInfo = [
          +dateFormat(null, 'MM'),
          +dateFormat(null, 'dd'),
          6 - new Date().getDay(),
        ];
        const clouldResult = await CloudSendSms(phoneList, dayInfo);
        this.logger.debug(clouldResult);
        const result = `${dayInfo[0]}月${dayInfo[1]}日
                      早上好，打工人！工作再累，一定不要忘记摸鱼哦 有事没事起身去茶水间去厕所去廊道走两步 ·
                      距离周末还有${dayInfo[2]}天 ·
                      世上有两种最耀眼的光芒，一种是太阳，一种是打工人努力的模样 · `;
        if (clouldResult) {
          infoLogger.info([phoneList, result, result]);
        }
      }
    }
    if (idList.length) {
      await getConnection()
        .createQueryBuilder()
        .update(Sms)
        .set({ isTodaySend: 'active' })
        .where({ id: In(idList) })
        .execute();
    }
  }

  /**
   * 定时任务 每天0:00 重置sms表的isTodaySend字段为active
   */
  @Cron('0 0 0 * * *')
  async resetSms() {
    const query = this.repository.createQueryBuilder('sms');
    const [data, total] = await query.getManyAndCount();
    await getConnection()
      .createQueryBuilder()
      .update(Sms)
      .set({ isTodaySend: 'locked' })
      .where({ id: In(data.map((v) => v.id)) })
      .execute();
  }
}
