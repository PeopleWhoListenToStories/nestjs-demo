import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { responseLogger } from '../logger';
interface IResponse<T> {
  data: T;
}

@Injectable()
export class ExcludeNullInterceptor<T>
  implements NestInterceptor<T, IResponse<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler<T>,
  ): Observable<IResponse<T>> {
    return next.handle().pipe(
      map((value) => {
        const ctx = context.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();

        const url = request.originalUrl;
        const statusCode = response.statusCode;

        const res = {
          statusCode,
          msg: null,
          success: true,
          data: value,
        };

        responseLogger.info(url, res);
        return res;
      }),
    );
  }
}
